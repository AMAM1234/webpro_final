<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    //use HasFactory;
    protected $primaryKey = 'student_id';
    protected $fillable = ['student_id','first_name', 'last_name','course_id'];
    protected $foreignkey = 'course_id';
}

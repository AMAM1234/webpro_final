<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class course extends Model
{
    //use HasFactory;
    protected $primaryKey = 'course_id';
    protected $fillable=['course_id','name_thai','name_eng'];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\students;
use App\Models\course;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = students::all()->toArray();
        return view('student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'course_id'     =>  'required'
        ]);
        $student = new students([
            'first_name'    =>  $request->get('first_name'),
            'last_name'     =>  $request->get('last_name'),
            'course_id'     =>  $request->get('course_id')
        ]);
        $student->save();
        return redirect()->route('student.create')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = students::find($id);
        return view('student.edit', compact('student', 'id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'course_id'     =>  'required'
        ]);
        $student = students::find($id);
        $student->first_name = $request->get('first_name');
        $student->last_name = $request->get('last_name');
        $student->course_id = $request->get('course_id');
        $student->save();
        return view('student.search')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = students::find($id);
        $student->delete();
        return view('student.search')->with('success', 'Data Deleted');
    }

    public function search(Request $request){
        $course_id = $request->get('course_id');
       
        $result = DB::select( DB::raw("SELECT students.first_name,students.last_name,students.student_id,courses.name_thai,courses.name_eng
                                             FROM examfinal.students 
                                             INNER JOIN examfinal.courses
                                             ON(students.course_id=courses.course_id)
                                             WHERE students.course_id=$course_id"));
        $result = collect($result)->map(function($x){ return (array) $x; })->toArray();
        // dd($namethai);
        return view('student.index',compact('result'))->with('course_id',$course_id);
    }
}
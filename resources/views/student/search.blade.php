@extends('layouts.master')
@section('title','Web Programming')
@section('content')

<br><br>
    <h1 align="center">Search Course</h1><hr>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-6"><br>
        
        <form method="post" action="{{url('/search')}}" class="form-inline">
         {{ csrf_field() }}
                <div class="form-group"  style="margin-left:40px">
                    <lable>กรุณากรอกคอร์สเรียน : &nbsp;</lable>
                    <input type="text" name="course_id" class="form-control" placeholder="โปรดระบุรหัสคอร์สเรียน"/>
                </div>
            <input class="btn btn-primary" type="submit" name="search" value="ค้นหา" style="margin-left:10px"/>
        </form>
        </div>
    </div>
</div>
        
@endsection
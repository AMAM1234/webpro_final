@extends('layouts.master')
@section('title','Web Programming')
@section('content')

<div class="row">
 <div class="col-md-12">
  <br />
 <h3 align="center">Member in Course</h3>
  <br />

  @if($message = Session::get('success'))
  <div class="alert alert-success">
   <p>{{$message}}</p>
  </div>
  @endif

  <div align="right" style="padding-right: 260px;">
        <a href="{{route('student.create')}}" class="btn btn-success">Add Members</a>
        <a href="{{url('/')}}" class="btn btn-primary" >Back to Search</a>
        <br />
        <br />
  </div>
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>Student ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Course Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        @foreach($result as $row)
        <tr>
            <td>{{$row['student_id']}}</td>
            <td>{{$row['first_name']}}</td>
            <td>{{$row['last_name']}}</td>
            <td>{{$row['name_thai']}}</td>
            <td>
                <a href="{{ route('student.edit', $row['student_id'])}}" class="btn btn-warning">Edit</a>
            </td>
            <td>
            <form method="post" class="delete_form" action="{{ route('student.destroy', $row['student_id'])}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE" />
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </td>
        </tr>
        @endforeach
        </table>
    </div>
    <div class="col-md-2"></div>
  </div>
 </div>
</div>

<script>
$(document).ready(function(){
 $('.delete_form').on('submit', function(){
  if(confirm("Are you sure you want to delete it?")){  
    return true;  
  }
  else  { 
    return false;
  }
 });
});
</script>
@endsection
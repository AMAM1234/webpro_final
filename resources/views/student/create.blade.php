@extends('layouts.master')

@section('content')
<div class="row">
  <div class="col-md-12">
    <br />
    <h3 align="center">Add Member</h3>
    <br />

    @if(count($errors) > 0)
    <div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
    </div>
    @endif

    @if(\Session::has('success'))
    <div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
    </div>
    @endif

    <form method="post" action="{{url('student')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" />
                    </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" />
                    </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="course_id" class="form-control" placeholder="Enter course id" />
                    </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div align="right" style="padding-right: 400px;">
                <div class="form-group">
                    <input type="submit"value="Create" class="btn btn-success" />&nbsp;
                    <a href="{{url('/')}}" class="btn btn-primary">Go to Search</a>
                </div>
            <br />
            <br />
        </div>
    </form>
 </div>
</div>
@endsection
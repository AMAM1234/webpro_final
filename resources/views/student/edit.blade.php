@extends('layouts.master')

@section('content')

<div class="row">
  <div class="col-md-12">
       <br />
       <h3 align="center">Edit Data of Member</h3>
       <br />
       @if(count($errors) > 0)

       <div class="alert alert-danger">
              <ul>
              @foreach($errors->all() as $error)
              <li>{{$error}}</li>
              @endforeach
              </ul>
       @endif
       <form method="post" action="{{ route('student.update', $id)}}">
              {{csrf_field()}}
              <input type="hidden" name="_method" value="PATCH" />
              <div class="row">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                            <div class="form-group">
                                   <input type="text" name="first_name" class="form-control" value="{{$student->first_name}}" placeholder="Enter First Name" />
                            </div>
                     </div>
                     <div class="col-md-4"></div>
              </div>

              <div class="row">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                            <div class="form-group">
                                   <input type="text" name="last_name" class="form-control" value="{{$student->last_name}}" placeholder="Enter Last Name" />
                            </div>
                     </div>
                     <div class="col-md-4"></div>
              </div>

              <div class="row">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                            <div class="form-group">
                                   <input type="text" name="course_id" class="form-control" value="{{$student->course_id}}" placeholder="Enter course id" />
                            </div>
                     </div>
                     <div class="col-md-4"></div>
              </div>

              <div align="right" style="padding-right: 400px;">
                     <div class="form-group">
                         <input type="submit" class="btn btn-warning" value="Edit" />&nbsp;
                         <a href="{{url('/')}}" class="btn btn-primary">Go to Search</a>
                     </div>
                 <br />
                 <br />
             </div>
       </form>
  </div>
</div>

@endsection
